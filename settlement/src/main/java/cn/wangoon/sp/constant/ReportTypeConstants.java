package cn.wangoon.sp.constant;

/**
 * @author MacGrady
 */
public interface ReportTypeConstants {
    String SETTLEMENT_REPORT_TYPE = "GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_V2";
}
