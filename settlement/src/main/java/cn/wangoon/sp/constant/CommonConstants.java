package cn.wangoon.sp.constant;

/**
 * @author MacGrady
 */
public interface CommonConstants {
    Integer SUCCESS = 0;
    Integer FAIL = 1;
    String DEFAULT_PASSWORD = "123456";
    String TOKEN_HEADER = "Authorization";
    String ACCESS_TOKEN = "access_token";
    String BEARER_TYPE = "Bearer";
    Integer CATALOG = -1;
    String MENU = "1";
    String PERMISSION = "2";
    String ADMIN_USER_NAME = "admin";
    Long WAREHOUSE_ID = 1L;
    String THREAD_POOL_BEAN = "taskExecutor";
    Long MENU_TREE_ROOT_ID = -1L;
    String LANGUAGE_HEADER = "x-language-header";
    String LOCK_KEY_PREFIX = "LOCK_KEY:";
    String FEEDREQUESTREPORT = "REPORTS";
    String FEEDREQUESTFEED = "FEEDS";
}
