package cn.wangoon.sp.config;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author MacGrady
 */
@Configuration
public class XxlJobConfig {
    @Value(value="${xxlValue.adminAddress:http://172.16.3.250:85/job-admin}")
    private String adminAddress;

    @Value(value = "${server.app-name:platform-api}")
    private String appName;

    @Value(value = "${xxlValue.token:3db8d72416263476274701998e}")
    private String token;

    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(adminAddress);
        xxlJobSpringExecutor.setAppname(appName);
        xxlJobSpringExecutor.setPort(7009);
        xxlJobSpringExecutor.setAccessToken(token);
        xxlJobSpringExecutor.setLogPath("/usr/local/temp/logs/logs.wangoon.cn/xxl-job/jobhandler");
        xxlJobSpringExecutor.setLogRetentionDays(10);
        return xxlJobSpringExecutor;
    }
}
