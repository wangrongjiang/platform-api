package cn.wangoon.sp.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author MacGrady
 */
public class TimeConvertUtil {

    /**
     * 获取当前月份的第一天
     * @return
     */
    public static String getFirstDateOfThisMonth(){
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return myFormatter.format(cal.getTime());
    }

    /**
     * 获取当前月份的最后一天
     * @return
     */
    public static String getLastDateOfThisMonth(){
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        cal.roll(Calendar.DATE, -1);
        return myFormatter.format(cal.getTime());
    }
}
