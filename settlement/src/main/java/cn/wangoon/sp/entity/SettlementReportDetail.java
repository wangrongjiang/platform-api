package cn.wangoon.sp.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


/**
 * 结算报告详情表
 * 数据库实体类，与数据库字段一一对应
 * @author MacGrady
 * @date 2022-03-26 11:31:08
 */
@Getter
@Setter
@TableName("v9_v3_settlement_report_detail")
public class SettlementReportDetail implements Serializable {

	private static final long serialVersionUID = 1L;

  	  		
	/**
	 * 
	 */
	private String settlementId;

	  		
	/**
	 * 
	 */
	private String settlementStartDate;

	  		
	/**
	 * 
	 */
	private String settlementEndDate;

	  		
	/**
	 * 
	 */
	private String depositDate;

	  		
	/**
	 * 
	 */
	private String totalAmount;

	  		
	/**
	 * 
	 */
	private String currency;

	  		
	/**
	 * 
	 */
	private String transactionType;

	  		
	/**
	 * 
	 */
	private String orderId;

	  		
	/**
	 * 
	 */
	private String merchantOrderId;

	  		
	/**
	 * 
	 */
	private String adjustmentId;

	  		
	/**
	 * 
	 */
	private String shipmentId;

	  		
	/**
	 * 
	 */
	private String marketplaceName;

	  		
	/**
	 * 
	 */
	private String amountType;

	  		
	/**
	 * 
	 */
	private String amountDescription;

	  		
	/**
	 * 
	 */
	private String amount;

	  		
	/**
	 * 
	 */
	private String fulfillmentId;

	  		
	/**
	 * 
	 */
	private String postedDate;

	  		
	/**
	 * 
	 */
	private String postedDateTime;

	  		
	/**
	 * 
	 */
	private String orderItemCode;

	  		
	/**
	 * 
	 */
	private String merchantOrderItemId;

	  		
	/**
	 * 
	 */
	private String merchantAdjustmentItemId;

	  		
	/**
	 * 
	 */
	private String sku;

	  		
	/**
	 * 
	 */
	private String quantityPurchased;

	  		
	/**
	 * 
	 */
	private String reportId;

	@Override
	public String toString() {
		return "SettlementReportDetail{" +
				"settlementId='" + settlementId + '\'' +
				", settlementStartDate='" + settlementStartDate + '\'' +
				", settlementEndDate='" + settlementEndDate + '\'' +
				", depositDate='" + depositDate + '\'' +
				", totalAmount='" + totalAmount + '\'' +
				", currency='" + currency + '\'' +
				", transactionType='" + transactionType + '\'' +
				", orderId='" + orderId + '\'' +
				", merchantOrderId='" + merchantOrderId + '\'' +
				", adjustmentId='" + adjustmentId + '\'' +
				", shipmentId='" + shipmentId + '\'' +
				", marketplaceName='" + marketplaceName + '\'' +
				", amountType='" + amountType + '\'' +
				", amountDescription='" + amountDescription + '\'' +
				", amount='" + amount + '\'' +
				", fulfillmentId='" + fulfillmentId + '\'' +
				", postedDate='" + postedDate + '\'' +
				", postedDateTime='" + postedDateTime + '\'' +
				", orderItemCode='" + orderItemCode + '\'' +
				", merchantOrderItemId='" + merchantOrderItemId + '\'' +
				", merchantAdjustmentItemId='" + merchantAdjustmentItemId + '\'' +
				", sku='" + sku + '\'' +
				", quantityPurchased='" + quantityPurchased + '\'' +
				", reportId='" + reportId + '\'' +
				'}';
	}
}
