package cn.wangoon.sp.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


/**
 * 结算报告下载总表
 * 数据库实体类，与数据库字段一一对应
 * @author MacGrady
 * @date 2022-03-25 10:44:55
 */
@Getter
@Setter
@TableName("v9_v3_settlement_report")
public class V3SettlementReport {

	private static final long serialVersionUID = 1L;

  	  		
	/**
	 * 市场ID
	 */
	private String marketplaceIds;

	  		
	/**
	 * 报告下载ID
	 */
	@TableId(value = "report_id")
	private String reportId;

	  		
	/**
	 * 报告类型
	 */
	private String reportType;

	  		
	/**
	 * 数据开始时间
	 */
	private String dataStartTime;

	  		
	/**
	 * 数据结束时间
	 */
	private String dataEndTime;

	  		
	/**
	 * 报告计划
	 */
	private String reportScheduled;

	  		
	/**
	 * 创建时间
	 */
	private String createdTime;

	  		
	/**
	 * 报告状态
	 */
	private String processingStatus;

	  		
	/**
	 * 请求开始时间
	 */
	private String processingStartTime;

	  		
	/**
	 * 请求结束时间
	 */
	private String processingEndTime;

	  		
	/**
	 * 报告文档地址
	 */
	private String reportDocumentId;

	private String shopSite;

	private LocalDateTime addTime;

	private LocalDateTime updateTime;

	private Integer status;

	private String fileUrl;

	private String msg;

	@Override
	public String toString() {
		return "V3SettlementReport{" +
				"marketplaceIds='" + marketplaceIds + '\'' +
				", reportId='" + reportId + '\'' +
				", reportType='" + reportType + '\'' +
				", dataStartTime='" + dataStartTime + '\'' +
				", dataEndTime='" + dataEndTime + '\'' +
				", reportScheduled='" + reportScheduled + '\'' +
				", createdTime='" + createdTime + '\'' +
				", processingStatus='" + processingStatus + '\'' +
				", processingStartTime='" + processingStartTime + '\'' +
				", processingEndTime='" + processingEndTime + '\'' +
				", reportDocumentId='" + reportDocumentId + '\'' +
				", shopSite='" + shopSite + '\'' +
				", addTime=" + addTime +
				", updateTime=" + updateTime +
				", status=" + status +
				", fileUrl='" + fileUrl + '\'' +
				'}';
	}
}
