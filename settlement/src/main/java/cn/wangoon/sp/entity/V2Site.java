package cn.wangoon.sp.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


/**
 * 
 * 数据库实体类，与数据库字段一一对应
 * @author MacGrady
 * @date 2022-03-24 11:32:32
 */
@Getter
@Setter
@TableName("v9_v2_site")
public class V2Site implements Serializable {

	private static final long serialVersionUID = 1L;

  	  		
	/**
	 * 站点Code
	 */
	private String code;

	  		
	/**
	 * 站点名称
	 */
	private String name;

	  		
	/**
	 * 币种
	 */
	private String currency;

	  		
	/**
	 * 
	 */
	private String salesChannel;

	  		
	/**
	 * 
	 */
	private Integer status;

	  		
	/**
	 * 站点ID
	 */
	private Integer siteId;

	  		
	/**
	 * 店铺ID
	 */
	private Integer shopId;

	  		
	/**
	 * 平台ID
	 */
	private Integer platformId;

	  		
	/**
	 * 开通API接口 1 开通 2 未开通
	 */
	private Integer apiOpen;

	  
}
