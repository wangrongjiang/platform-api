package cn.wangoon.sp.query;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 结算报告详情表
 * 分页查询封装的对象，只有一个查询参数删除此类
 * @author MacGrady
 * @date 2022-03-26 11:31:08
 */
@Getter
@Setter
public class SettlementReportDetailQuery implements Serializable {

	private static final long serialVersionUID = 1L;

}
