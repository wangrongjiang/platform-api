package cn.wangoon.sp.query;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 
 * 分页查询封装的对象，只有一个查询参数删除此类
 * @author MacGrady
 * @date 2022-03-24 11:32:32
 */
@Getter
@Setter
public class V2SiteQuery implements Serializable {

	private static final long serialVersionUID = 1L;

}
