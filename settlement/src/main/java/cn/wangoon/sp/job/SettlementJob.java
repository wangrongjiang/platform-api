package cn.wangoon.sp.job;

import cn.wangoon.sp.constant.ReportTypeConstants;
import cn.wangoon.sp.service.AmazonSpService;
import cn.wangoon.sp.service.V2SiteService;
import cn.wangoon.sp.service.V3SettlementReportService;
import cn.wangoon.sp.vo.ShopSiteVo;
import cn.wangoon.sp.vo.V3SettlementReportVO;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 亚马逊结算报告下载
 *
 * @author MacGrady
 */
@Component
@Slf4j
@RestController
public class SettlementJob {
    private static final Logger logger = LoggerFactory.getLogger(SettlementJob.class);
    @Resource
    private AmazonSpService amazonSpService;
    @Resource
    private V2SiteService v2SiteService;


    @Resource
    private V3SettlementReportService v3SettlementReportService;

    /**
     * 获取当前月份的已经生成的报告
     */
    @XxlJob("SettlementRequest")
    public void settlementRequest() {
        /**获取要开启的站点列表**/
        List<ShopSiteVo> shopSiteVos = v2SiteService.selectShopAndSite(null);
        /** 调用接口获取已经生成的结算报告 */
        amazonSpService.getReports(shopSiteVos, ReportTypeConstants.SETTLEMENT_REPORT_TYPE);
    }

    /**
     * 创建下载请求
     */
    @XxlJob("getFeedOrReportResultByIdAndType")
    public void getFeedOrReportResultByIdAndType() {
        Map<String, Integer> selectMap = new HashMap<>(2);
        selectMap.put("status",1);
        selectMap.put("limitNumber",10);
        log.info(selectMap.toString());
        List<V3SettlementReportVO> settlements = v3SettlementReportService.getSettlementByStatus(selectMap);
        amazonSpService.getFeedOrReportResultByIdAndType(settlements);
    }

    /**
     * 下载结算报表详情
     */
    @XxlJob("getReports")
    public void getReports() {
        Map<String, Integer> selectMap = new HashMap<>(2);
        selectMap.put("status",2);
        selectMap.put("limitNumber",1);
        List<V3SettlementReportVO> v3SettlementReports = v3SettlementReportService.getSettlementByStatus(selectMap);
        List<String> reportIds = new ArrayList<>();
        v3SettlementReports.stream().forEach(i -> {
            reportIds.add(i.getReportId());
        });
        v3SettlementReportService.setDownloadingStatusByReportIds(reportIds);
        amazonSpService.getReportContent(v3SettlementReports);
    }
}
