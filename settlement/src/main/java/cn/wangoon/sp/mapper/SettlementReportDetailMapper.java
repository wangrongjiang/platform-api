package cn.wangoon.sp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.wangoon.sp.entity.SettlementReportDetail;
import cn.wangoon.sp.vo.SettlementReportDetailVO;
import cn.wangoon.sp.query.SettlementReportDetailQuery;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;


/**
 * 结算报告详情表
 *
 * @author MacGrady
 * @date 2022-03-26 11:31:08
 */
public interface SettlementReportDetailMapper extends BaseMapper<SettlementReportDetail> {

    IPage<SettlementReportDetailVO> listByPage(Page<SettlementReportDetailQuery> page, @Param("query") SettlementReportDetailQuery settlementReportDetailQuery);

    void deleteByReportId(String reportId);
}
