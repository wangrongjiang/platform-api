package cn.wangoon.sp.mapper;

import cn.wangoon.sp.entity.V3SettlementReport;
import cn.wangoon.sp.vo.V3SettlementReportVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


/**
 * 结算报告下载总表
 *
 * @author MacGrady
 * @date 2022-03-25 10:44:55
 */
public interface V3SettlementReportMapper extends BaseMapper<V3SettlementReport> {

    public int batchDeleteByReportId(List<String> reportIds);

    public List<V3SettlementReportVO> getSettlementByStatus(Map map);

    public void setDownloadingStatusByReportIds(List<String> reportIds);
}
