package cn.wangoon.sp.mapper;

import cn.wangoon.sp.entity.V2Site;
import cn.wangoon.sp.vo.ShopSiteVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;


/**
 * 
 *
 * @author MacGrady
 * @date 2022-03-24 11:32:32
 */
public interface V2SiteMapper extends BaseMapper<V2Site> {

    /**
     * 获取站点店铺
     * @param siteId
     * @return
     */
    List<ShopSiteVo> selectShopAndSite(Integer siteId);



}
