package cn.wangoon.sp.service;

import cn.wangoon.sp.entity.SettlementReportDetail;

import java.util.List;

/**
 * 结算报告详情表
 *
 * @author MacGrady
 * @date 2022-03-26 11:31:08
 */
public interface SettlementReportDetailService {

    public void insertBatch(List<SettlementReportDetail> settlementReportDetails);

    public void deleteByReportId(String reportId);

}
