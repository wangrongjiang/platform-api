package cn.wangoon.sp.service;

import cn.wangoon.sp.vo.ShopSiteVo;

import java.util.List;

/**
 * 
 *
 * @author MacGrady
 * @date 2022-03-24 11:32:32
 */
public interface V2SiteService {

    List<ShopSiteVo> selectShopAndSite(Integer siteId);
}
