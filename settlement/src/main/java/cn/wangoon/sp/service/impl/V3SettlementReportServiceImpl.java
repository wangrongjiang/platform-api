package cn.wangoon.sp.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.wangoon.sp.constant.CommonConstants;
import cn.wangoon.sp.entity.V3SettlementReport;
import cn.wangoon.sp.mapper.V3SettlementReportMapper;
import cn.wangoon.sp.service.V3SettlementReportService;
import cn.wangoon.sp.vo.V3SettlementReportVO;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 结算报告下载总表
 *
 * @author MacGrady
 * @date 2022-03-25 10:44:55
 */
@AllArgsConstructor
@Service
public class V3SettlementReportServiceImpl extends ServiceImpl<V3SettlementReportMapper, V3SettlementReport> implements V3SettlementReportService {

    private V3SettlementReportMapper v3SettlementReportMapper;

    @Async(CommonConstants.THREAD_POOL_BEAN)
    @Override
    public void batchInsert(List<V3SettlementReport> items) {
        if (CollUtil.isNotEmpty(items)) {
            this.saveBatch(items, items.size());
        }
    }

    @Override
    public void saveSettlement(V3SettlementReport item) {
        this.save(item);
    }

    @Override
    public int batchDeleteByReportId(List<String> reportIds) {
        return v3SettlementReportMapper.batchDeleteByReportId(reportIds);
    }

    @Async(CommonConstants.THREAD_POOL_BEAN)
    @Override
    public void saveOrUpdateBatchParams(List<V3SettlementReport> items) {
        if (CollUtil.isNotEmpty(items)) {
            this.saveOrUpdateBatch(items);
        }
    }

    @Override
    public void saveOrUpdateParams(V3SettlementReport v3SettlementReport){
        this.saveOrUpdate(v3SettlementReport);
    }

    @Override
    public List<V3SettlementReportVO> getSettlementByStatus(Map map){
        return v3SettlementReportMapper.getSettlementByStatus(map);
    }

    @Override
    public void updateSettlementById(V3SettlementReport item) {
        this.updateById(item);
    }

    @Override
    public void deleteByReportId(String reportId) {
        this.removeById(reportId);
    }

    @Override
    public void setDownloadingStatusByReportIds(List<String> reportIds) {
        v3SettlementReportMapper.setDownloadingStatusByReportIds(reportIds);
    }
}
