package cn.wangoon.sp.service;

import cn.wangoon.sp.vo.ShopSiteVo;
import cn.wangoon.sp.vo.V3SettlementReportVO;

import java.util.List;

/**
 * @author MacGrady
 */
public interface AmazonSpService {
    /**
     * 获取GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_V2已生成的报告
     * 状态是 1
     * @param reportType
     * @param shopSiteVos
     * @return
     */
    void getReports(List<ShopSiteVo> shopSiteVos,String reportType);

    /**
     * 根据id获取报告或feed结果 type:REPORTS或FEEDS
     * 获取到ZIP下载地址以后状态是 2
     * @param v3SettlementReports
     */
    void getFeedOrReportResultByIdAndType(List<V3SettlementReportVO> v3SettlementReports);

    /**
     * 下载结算报表详情
     * @param v3SettlementReports
     */
    void getReportContent(List<V3SettlementReportVO> v3SettlementReports);
}
