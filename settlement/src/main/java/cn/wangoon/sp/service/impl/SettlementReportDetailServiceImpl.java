package cn.wangoon.sp.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.wangoon.sp.constant.CommonConstants;
import cn.wangoon.sp.entity.SettlementReportDetail;
import cn.wangoon.sp.mapper.SettlementReportDetailMapper;
import cn.wangoon.sp.service.SettlementReportDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 结算报告详情表
 *
 * @author MacGrady
 * @date 2022-03-26 11:31:08
 */
@AllArgsConstructor
@Service
public class SettlementReportDetailServiceImpl extends ServiceImpl<SettlementReportDetailMapper, SettlementReportDetail> implements SettlementReportDetailService {

    @Resource
    private SettlementReportDetailMapper settlementReportDetailMapper;

    @Async(CommonConstants.THREAD_POOL_BEAN)
    @Override
    public void insertBatch(List<SettlementReportDetail> settlementReportDetails) {
        if (CollUtil.isNotEmpty(settlementReportDetails)){
            this.saveBatch(settlementReportDetails,settlementReportDetails.size());
        }
    }

    @Override
    public void deleteByReportId(String reportId) {
        settlementReportDetailMapper.deleteByReportId(reportId);
    }
}
