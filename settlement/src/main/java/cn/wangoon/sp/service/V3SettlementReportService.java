package cn.wangoon.sp.service;

import cn.wangoon.sp.entity.V3SettlementReport;
import cn.wangoon.sp.vo.V3SettlementReportVO;

import java.util.List;
import java.util.Map;

/**
 * 结算报告下载总表
 *
 * @author MacGrady
 * @date 2022-03-25 10:44:55
 */
public interface V3SettlementReportService {
    /**
     * 批量保存
     * @param items
     */
    public void batchInsert(List<V3SettlementReport> items);

    public void saveSettlement(V3SettlementReport item);

    public int batchDeleteByReportId(List<String> reportIds);

    public void saveOrUpdateBatchParams(List<V3SettlementReport> items);

    public void saveOrUpdateParams(V3SettlementReport v3SettlementReport);

    public List<V3SettlementReportVO> getSettlementByStatus(Map map);

    public void updateSettlementById(V3SettlementReport item);

    public void deleteByReportId(String reportId);

    public void setDownloadingStatusByReportIds(List<String> reportIds);
}
