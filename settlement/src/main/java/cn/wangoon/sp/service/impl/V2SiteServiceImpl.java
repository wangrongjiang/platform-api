package cn.wangoon.sp.service.impl;

import cn.wangoon.sp.mapper.V2SiteMapper;
import cn.wangoon.sp.service.V2SiteService;
import cn.wangoon.sp.vo.ShopSiteVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 *
 * @author MacGrady
 * @date 2022-03-24 11:32:32
 */
@AllArgsConstructor
@Service
public class V2SiteServiceImpl implements V2SiteService {
    private V2SiteMapper v2SiteMapper;
    @Override
    public List<ShopSiteVo> selectShopAndSite(Integer siteId){
        return v2SiteMapper.selectShopAndSite(siteId);
    }

}
