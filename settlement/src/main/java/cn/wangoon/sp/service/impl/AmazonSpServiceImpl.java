package cn.wangoon.sp.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.wangoon.sp.constant.CommonConstants;
import cn.wangoon.sp.entity.SettlementReportDetail;
import cn.wangoon.sp.entity.V3SettlementReport;
import cn.wangoon.sp.service.AmazonSpService;
import cn.wangoon.sp.service.SettlementReportDetailService;
import cn.wangoon.sp.service.V3SettlementReportService;
import cn.wangoon.sp.util.TimeConvertUtil;
import cn.wangoon.sp.vo.ShopSiteVo;
import cn.wangoon.sp.vo.V3SettlementReportVO;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author MacGrady
 */
@Service
@Slf4j
public class AmazonSpServiceImpl implements AmazonSpService {
    @Value(value = "${wanGoon.api-url}")
    private String aws_host;
    @Value(value = "${wanGoon.file-url}")
    private String file_host;
    private static final String HEADER_SITE = "x-sitecode-header";
    private static final String HEADER_SYS = "x-SystemCode-header";
    public static final String CODE = "code";
    private static int RESPONSE_SUCCESS_CODE = 200;
    private int total = 0;

    @Autowired
    private V3SettlementReportService v3SettlementReportService;

    @Autowired
    private SettlementReportDetailService settlementReportDetailService;

    @Resource(name = CommonConstants.THREAD_POOL_BEAN)
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Override
    public void getReports(List<ShopSiteVo> shopSiteVos, String reportType) {
        shopSiteVos.stream().forEach(shopSiteVo -> {
            threadPoolTaskExecutor.execute(() -> {
                String shopSite = StrUtil.concat(true, shopSiteVo.getShopName(), StrUtil.UNDERLINE, shopSiteVo.getSiteName());
                StringBuffer params = new StringBuffer();
                params.append("createdSince=");
                params.append(StrUtil.concat(true, TimeConvertUtil.getFirstDateOfThisMonth(), "T08:00:00%2B01:00"));
                params.append("&createdUntil=");
                params.append(StrUtil.concat(true, TimeConvertUtil.getLastDateOfThisMonth(), "T07:59:59%2B01:00"));
                params.append("&siteCodes=").append(shopSiteVo.getSiteName());
                params.append("&reportTypes=").append(reportType);
                params.append("&processingStatuses=").append("DONE");
                log.info(aws_host + "amazon-sp/api/reports/getReports?" + params.toString());
                HttpResponse response = HttpUtil.createGet(aws_host + "amazon-sp/api/reports/getReports?" + params.toString())
                        .timeout(30 * 1000)
                        .header(HEADER_SITE, shopSite)
                        .header(HEADER_SYS, "ERP")
                        .execute();
                String responseBody = response.body();
                log.info("请求【{}】当前报告列表: {}", reportType, responseBody);
                JSONObject jsonObject = JSONObject.parseObject(responseBody);
                if (response.isOk() && jsonObject.getInteger(CODE) == 0) {
                    JSONObject datas = jsonObject.getJSONObject("datas");
                    if (datas != null) {
                        List<V3SettlementReportVO> items = datas.getJSONArray("payload").toJavaList(V3SettlementReportVO.class);
                        List<String> reportIds = new ArrayList<>();
                        List<V3SettlementReport> v3SettlementReports = items.stream().map(i -> {
                            V3SettlementReport v3SettlementReport = new V3SettlementReport();
                            v3SettlementReport.setMarketplaceIds(Arrays.toString(i.getMarketplaceIds().toArray()));
                            v3SettlementReport.setReportId(i.getReportId());
                            v3SettlementReport.setReportType(i.getReportType());
                            v3SettlementReport.setDataStartTime(i.getDataStartTime());
                            v3SettlementReport.setDataEndTime(i.getDataEndTime());
                            v3SettlementReport.setReportScheduled(i.getReportScheduled());
                            v3SettlementReport.setCreatedTime(i.getCreatedTime());
                            v3SettlementReport.setProcessingStatus(i.getProcessingStatus());
                            v3SettlementReport.setProcessingStartTime(i.getProcessingStartTime());
                            v3SettlementReport.setProcessingEndTime(i.getProcessingEndTime());
                            v3SettlementReport.setReportDocumentId(i.getReportDocumentId());
                            v3SettlementReport.setShopSite(shopSite);
                            v3SettlementReport.setAddTime(LocalDateTime.now());
                            v3SettlementReport.setUpdateTime(LocalDateTime.now());

                            reportIds.add(i.getReportId());
                            return v3SettlementReport;
                        }).collect(Collectors.toList());
                        if (CollUtil.isNotEmpty(v3SettlementReports)) {
                            v3SettlementReportService.saveOrUpdateBatchParams(v3SettlementReports);
                        }
                    }
                }
            });

        });
    }

    @Override
    public void getFeedOrReportResultByIdAndType(List<V3SettlementReportVO> v3SettlementReports) {
        v3SettlementReports.stream().forEach(v3SettlementReport -> {
            threadPoolTaskExecutor.execute(() -> {
                StringBuffer params = new StringBuffer();
                params.append("feedId=").append(v3SettlementReport.getReportId());
                params.append("&reportType=").append(v3SettlementReport.getReportType());
                params.append("&type=").append(CommonConstants.FEEDREQUESTREPORT);
                String uri = aws_host + "amazon-sp/api/feeds/getFeedOrReportResultByIdAndType?" + params.toString();
                log.info(uri);
                HttpResponse response = HttpUtil.createGet(uri)
                        .timeout(30 * 1000)
                        .header(HEADER_SITE, v3SettlementReport.getShopSite())
                        .header(HEADER_SYS, "ERP")
                        .execute();
                String responseBody = response.body();
                log.info("请求【{}】当前报告列表: {}", v3SettlementReport.getReportType(), responseBody);
                JSONObject jsonObject = JSONObject.parseObject(responseBody);
                if (response.isOk() && jsonObject.getInteger(CODE) == 0) {
                    List<V3SettlementReport> datas = jsonObject.getJSONArray("datas").toJavaList(V3SettlementReport.class);

                    datas.stream().forEach(s -> {
                        if (s.getFileUrl() != null) {
                            s.setStatus(2);
                            s.setUpdateTime(LocalDateTime.now());
                            v3SettlementReportService.saveOrUpdateParams(s);
                        }
                    });
                }

            });
        });
    }

    @Override
    public void getReportContent(List<V3SettlementReportVO> v3SettlementReports) {
        v3SettlementReports.stream().forEach(v3SettlementReport -> {
            threadPoolTaskExecutor.execute(() -> {
                V3SettlementReport settlement = new V3SettlementReport();
                settlement.setReportId(v3SettlementReport.getReportId());
                settlement.setUpdateTime(LocalDateTime.now());
                total = 0;
                if (v3SettlementReport.getFileUrl() != null) {
                    try {
                        settlementReportDetailService.deleteByReportId(v3SettlementReport.getReportId());
                        List<SettlementReportDetail> content = getSettlementDetailListFromZipStream(file_host + v3SettlementReport.getFileUrl(), v3SettlementReport.getReportId());
                        settlement.setStatus(4);
                        settlement.setMsg("文件为空 下载行数是:" + String.valueOf(content.size()));
                        if (CollUtil.isNotEmpty(content)) {
                            settlementReportDetailService.insertBatch(content);
                            settlement.setStatus(4);
                            settlement.setMsg(String.valueOf(content.size() + total));
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        settlement.setStatus(5);
                        settlement.setMsg(exception.getMessage());
                    } finally {
                        v3SettlementReportService.updateSettlementById(settlement);
                    }
                } else {
                    settlement.setStatus(1);
                    settlement.setMsg("FileURL为空");
                    v3SettlementReportService.updateSettlementById(settlement);
                }
            });
        });
    }

    private List<SettlementReportDetail> getSettlementDetailListFromZipStream(String dnFileUrl,String reportId) throws IOException {
        ZipInputStream zipInputStream = null;
        BufferedReader br = null;
        URL url = new URL(dnFileUrl);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        Charset utf8 = Charset.forName("UTF-8");
        int resultCode = conn.getResponseCode();
        List<SettlementReportDetail> content = new ArrayList<>();
        if (resultCode == RESPONSE_SUCCESS_CODE) {
            zipInputStream = new ZipInputStream(conn.getInputStream(), utf8);
            ZipEntry zipEntry;
            SettlementReportDetail detail = null;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {

                if (zipEntry.toString().endsWith("txt")) {

                    br = new BufferedReader(new InputStreamReader(zipInputStream));
                    String line;
                    while ((line = br.readLine()) != null) {
                        if (stringToArray(reportId, content, line)) {
                            continue;
                        }
                        if (content.size() >= 20000){
                            total = total + content.size();
                            settlementReportDetailService.insertBatch(content);
                            content.clear();
                        }
                    }
                }
            }
        }
        return content;
    }

    public static boolean stringToArray(String reportId, List<SettlementReportDetail> content, String line) {
        SettlementReportDetail detail;
        String[] lineContent = line.split("\t");
        if (lineContent.length == 0 || StrUtil.equals(lineContent[0],"settlement-id")) {
            return true;
        }
        detail = new SettlementReportDetail();
        detail.setReportId(reportId);
        if (lineContent.length == 6) {
            detail.setSettlementId(lineContent[0]);
            detail.setSettlementStartDate(lineContent[1]);
            detail.setSettlementEndDate(lineContent[2]);
            detail.setDepositDate(lineContent[3]);
            detail.setTotalAmount(lineContent[4]);
            detail.setCurrency(lineContent[5]);
        }else if(lineContent.length == 23) {
            detail.setSettlementId(lineContent[0]);
            detail.setTransactionType(lineContent[6]);
            detail.setOrderId(lineContent[7]);
            detail.setMerchantOrderId(lineContent[8]);
            detail.setAdjustmentId(lineContent[9]);
            detail.setShipmentId(lineContent[10]);
            detail.setMarketplaceName(lineContent[11]);
            detail.setAmountType(lineContent[12]);
            detail.setAmountDescription(lineContent[13]);
            detail.setAmount(lineContent[14]);
            detail.setFulfillmentId(lineContent[15]);
            detail.setPostedDate(lineContent[16]);
            detail.setPostedDateTime(lineContent[17]);
            detail.setOrderItemCode(lineContent[18]);
            detail.setMerchantOrderItemId(lineContent[19]);
            detail.setMerchantAdjustmentItemId(lineContent[20]);
            detail.setSku(lineContent[21]);
            detail.setQuantityPurchased(lineContent[22]);
        }
        if (detail.getSettlementId() != null){
            content.add(detail);
        }
        return false;
    }
}
