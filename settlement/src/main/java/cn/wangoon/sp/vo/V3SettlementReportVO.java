package cn.wangoon.sp.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 结算报告下载总表
 * 新增，修改，查询返回数据封装的对象
 * @author MacGrady
 * @date 2022-03-25 10:44:55
 */
@Setter
@Getter
public class V3SettlementReportVO {

    private static final long serialVersionUID = 1L;

	private int id;
  	  		
	/**
	 * 市场ID
	 */
	@TableField(exist = false)
	private List<String> marketplaceIds;
	  		
	/**
	 * 报告下载ID
	 */
	@NotBlank(message="报告下载ID不能为空")
	private String reportId;
	  		
	/**
	 * 报告类型
	 */
	private String reportType;
	  		
	/**
	 * 数据开始时间
	 */
	private String dataStartTime;
	  		
	/**
	 * 数据结束时间
	 */
	private String dataEndTime;
	  		
	/**
	 * 报告计划
	 */
	private String reportScheduled;
	  		
	/**
	 * 创建时间
	 */
	private String createdTime;
	  		
	/**
	 * 报告状态
	 */
	private String processingStatus;
	  		
	/**
	 * 请求开始时间
	 */
	@NotBlank(message="请求开始时间不能为空")
	private String processingStartTime;
	  		
	/**
	 * 请求结束时间
	 */
	private String processingEndTime;
	  		
	/**
	 * 报告文档地址
	 */
	private String reportDocumentId;

	private String shopSite;

	private LocalDateTime addTime;

	private LocalDateTime updateTime;

	private Integer status;

	private String fileUrl;

	@Override
	public String toString() {
		return "V3SettlementReportVO{" +
				"marketplaceIds=" + marketplaceIds +
				", reportId='" + reportId + '\'' +
				", reportType='" + reportType + '\'' +
				", dataStartTime='" + dataStartTime + '\'' +
				", dataEndTime='" + dataEndTime + '\'' +
				", reportScheduled='" + reportScheduled + '\'' +
				", createdTime='" + createdTime + '\'' +
				", processingStatus='" + processingStatus + '\'' +
				", processingStartTime='" + processingStartTime + '\'' +
				", processingEndTime='" + processingEndTime + '\'' +
				", reportDocumentId='" + reportDocumentId + '\'' +
				", shopSite='" + shopSite + '\'' +
				", addTime=" + addTime +
				", updateTime=" + updateTime +
				", status=" + status +
				", fileUrl=" + fileUrl +
				'}';
	}
}
