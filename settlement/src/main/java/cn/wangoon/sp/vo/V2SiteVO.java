package cn.wangoon.sp.vo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * 
 * 新增，修改，查询返回数据封装的对象
 * @author MacGrady
 * @date 2022-03-24 11:32:32
 */
@Setter
@Getter
public class V2SiteVO {

    private static final long serialVersionUID = 1L;

  	  		
	/**
	 * 站点Code
	 */
	private String code;
	  		
	/**
	 * 站点名称
	 */
	private String name;
	  		
	/**
	 * 币种
	 */
	private String currency;
	  		
	/**
	 * 
	 */
	private String salesChannel;
	  		
	/**
	 * 
	 */
	private Integer status;
	  		
	/**
	 * 站点ID
	 */
	@NotNull (message="站点ID不能为空")
	private Integer siteId;
	  		
	/**
	 * 店铺ID
	 */
	private Integer shopId;
	  		
	/**
	 * 平台ID
	 */
	private Integer platformId;
	  		
	/**
	 * 开通API接口 1 开通 2 未开通
	 */
	private Integer apiOpen;
	  
}
