package cn.wangoon.sp.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author MacGrady
 */
@Getter
@Setter
public class ShopSiteVo {
    private String shopName;

    private String siteName;
}
